---
title: A Message from the Recorder of Deeds
layout: page
sidenav: false
---
# **{{page.title}}**

> The St. Louis City Recorder of Deeds Office proudly provides critical record keeping custodial services to the public that virtually touches every resident and business that lives and operates within our City limits.  We ARE the keepers of history by holding the city’s records and historical archives, and maintaining nearly 250 years’ worth of history on St. Louis people and property.  My staff and I are proud of the services we provide and commit that we will strive every day to maintain the most efficient and user friendly experience possible.   I invite you to visit our office either in person or online from the comfort your living room

> This Recorder of Deeds office is unique in that it is the only place in the state of Missouri where you can make one stop to have access to birth, marriage, death, land and old corporate records, all at one time in the same location.  We are committed to a state of the art, innovate and user focused service.

We look forward to servicing your needs.

Sincerely,
Michael Butler
