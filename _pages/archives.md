---
title: Archives
layout: page
call: 314-589-8174
email: archives@stlouiscityrecorder.org
mail: Archives Department, City Hall Room 129, 1200 Market Stree
sidenav: True
---
# **{{page.title}}**

 The Archives Department is the custodian of historic documents including birth and death records for St. Louis City, land records, marriage licenses , and articles of incorporation.

 This office also helps with genealogical searches and in locating documents for academic or scholarly purposes.

 _The Archives Department is located in Room 129 of City Hall and may be reached at (314) 589-6796._

 <section class="standout">
 <h2>Deed Copy Requirements</h2>
 <h3>1921 to Present</h3>
 <h4>No Refunds. No Exchanges After Document Leaves Office.</h4>
 <p><em>Customer is responsible for purchasing desired document copy.</em></p>
 <p><a href="https://mo4laredo.fidlar.com/MOStLouis/DirectSearch/#!/search">Free land history search</a></p>
 <h3>Copy Requirements – Fee and Information Customer Must Provide</h3>
 <ul>
 	<li>$ Payment for Walk-In Service is by Visa/MasterCard, Cash, Money Order, or Business Check. No personal checks.</li>
 	<li>$ Payment for Mail-In Service is by Personal Check, Business Check, Money Order, or Cash.</li>
 	<li>Fee for Certified: $5 First Page + $2.00 Each Additional Page</li>
 	<li>Fee for Uncertified: $3 First Page + $2.00 Each Additional Page</li>
 	<li>Provide Grantee Name (Buyer or Recipient)</li>
 	<li>Provide Property Address (Street Number + Street Name)</li>
 	<li>Provide Document’s Recorder Book and Page Numbers</li>
 </ul>
 </section>

### How to Obtain Book and Page Numbers

### Documents Recorded July 1982 to Present

_Customer Must Provide: Address or Grantee/Grantor Name_

* One Document: The Recorder's Archives Staff will search the Recorder’s Database (by Address, or Name, or Parcel Number) and provide customer with the Recorder Book and Page Numbers.
* Multiple Documents: Customer must use one of theRecorder’s public research computers to search the Recorder’s Database (by Address, or Name, or Parcel Number) and obtain Recorder Book and Page Numbers.
Document Recorded 1921 to July 1982 Customer Must Provide: Date and Daily Numbers
From Room 114, Assessor’s Office, City Hall
* One Document: Customer provides Recorder'sImaging/Microfilm Staff with Date and Daily Numbers from Assessor. Staff will search Recorder's Date and Daily Book to provide customer with the Recorder Book and Page Numbers.
* Multiple Documents: Customer obtains Date and DailyNumbers from Assessor, and then searches Recorder's Date and Daily Books to obtain the Recorder Book and Page Numbers.


<section class="standout">
<h3><span class="caps">ADOPTION</span> <span class="caps">DEEDS</span> (pre-1917)</h3>
<ul>
	<li>$0 Name Search Using Deeds of Adoptions Card File Index</li>
	<li>$3 Per Uncertified Copy</li>
	<li>No Copies of Index Cards or Book Covers<br>
</li></ul></section>



### LAND RECORDS – No Copies of Index

#### Search Fees
   * $20 per ten year search for Grantor Index search
   * $20 per ten year search for Grantee Index search

#### Copy Fees
   * $3 First Page Uncertified Copy
   * $2 Each Page Thereafter, Same Document
   * No Copies of Index Pages or Book Covers

<section class="standout">
   <h3><span class="caps">MARRIAGE</span> <span class="caps">RECORDS</span></h3>
   <a href="http://marriage.stlouiscityrecorder.org/DirectSearch/home.html">Free Marriage Search</a>
   <h4>Search Fees</h4>
   <ul>
   	<li>$4 Per Name for Bride Index search</li>
   	<li>$4 Per Name for Groom Index search <br>
   </li></ul>
</section>
