---
title: Land Records
layout: page
call: 314-622-3260
email: land@stlouiscityrecorder.org
mail: Land Records Dept., Recorder of Deeds, 1200 Market Street
sidenav: True
---

# {{page.title}}

p. Our Land Records Department receives and maintains all incoming real estate-related documents for land located within the City of St. Louis — deeds, liens, affidavits, court orders, and other miscellaneous recordable documents.

p. Documents received in this department must adhere to guidelines as outlined in Missouri State Statues. This department is fully automated with most information being accessible by our highly skilled staff within minutes in most cases.

_The Land Records Department is located in Room 126 of City Hall and may be reached at (314) 622-3259._


"Free land history search":https://mo4laredo.fidlar.com/MOStLouis/DirectSearch/#!/search

## Deed Recording and Fee Requirements.

<section class="standout">
<h3>Standard Page Format Requirements </h3>
<p><i>Paper:</i></p>
<ul>
	<li>8.5” x 11” white or light colored paper, not less than 20 lb. weight.</li>
	<li>No watermarks or visible inclusions.</li>
	<li>No permanently bound or in continuous form.</li>
	<li>No stapling or affixing of Attachments to any page except as necessary to comply with statutes.</li>
</ul>
<p><i>Type/Print:</i></p>
<ul>
	<li>Black or Dark Blue Ink not less than 8 point type.</li>
	<li>Printing on only one side of each page.</li>
</ul>
<p><i>Margins:</i></p>
<ul>
	<li>First Page: 3-Inch Blank Top Margin: 3-Inches of vertical space from left to right that is void of any text, writing, graphic.</li>
	<li>First Page: Bottom Margin &amp; Side Margins shall be ¾” to 1”.</li>
	<li>Additional Pages- Top, Bottom, Side Margins shall be ¾” to 1”.<br>
</li></ul>
</section>

### Exceptions to Page Format Requirements:

* Documents signed prior to January 1, 2002.
* Document executed outside United States.
* Document where one of original parties is deceased/incapacitated.
* Judgments or other documents formatted to meet court requirements.
* Uniform Commercial Code recordings.
* Federal and State Tax Liens and their Lien Releases.

### First Page Recording Requirements

* Affected property must be located in City of St. Louis.
* Title/Type of document (such as General Warranty Deed, Quit Claim Deed) must be stated in the heading.
* Origination Date of document must be stated (such as “made on this 4th day of July, 2002”).
* The Legal Names of All Grantors and All Grantees must be stated.
* The Mailing Address for Each Grantor must be stated.
* The Mailing Address for At Least One Grantee must be stated.
* Legal Description of Property must be correct, complete and include City Block Number + Lot Number.
* When a document refers to any previously recorded document, the new document must state the Book & Page Numbers or Date & Daily Numbers of the cited previous document.
* If insufficient space on the First Page for all aforementioned information, the First Page must include a citation for the Page Reference within the document, or the Exhibit or Attachment to the document, where the required information is provided.

### Miscellaneous Recording Requirements

p. Release for a Deed of Trust Recorded Before 1986: Must be accompanied by original Note and Deed of Trust.

p. Certificate of Value Required (Chapter 5.70 St. Louis City Revised Code). Contact St. Louis City Assessor at 314.622.3212.

<section class="standout">
<h2>Notarized Signatures Requirements</h2>
<ul>
 <li>All Parties to document <em>both Grantor and Grantee</em> Must Have Their Notarized Signatures on document (Chapter 15.152.030 St. Louis City Revised Code).</li>
 <li>Full Legal Names must be used.</li>
 <li>Deeds of Trust and Easements exempt from Grantee signature requirement.</li>
 <li>Under each Signature, the party’s Name Must be Legibly Typed or</li>
 <li>Printed sufficient for microfilming/scanning and reproduction of a legible copy.</li>
 <li>Signature and typed/printed name must be same full legal name.</li>
 <li>The Notary Seal or Stamp Must Be in Black Ink.</li>
 <li>The Notary Seal or Stamp Must Be Imprinted Under the Notary’s</li>
 <li>Signature and must be legible sufficient for microfilming/scanning and reproduction of a legible copy. It must read, as example:</li>
</ul>
<blockquote>
<p>Jane Doe<br>
Notary Public<br>
Notary Seal<br>
State of Missouri Notary Commission Number (if applicable)</p>
</blockquote>
<p>The Notary’s “Commission Expiration Date” Must Be Clearly<br>
Imprinted on the document from the Notary Seal or Stamp.</p>
</section>

### Recording Fees –Payment Due In Advance of Services

p. __Payment for Walk-In Service:__ No Personal Checks. Pay with Cash, Money Order, or Business Check.

p. __Payment for Mail-In Service:__ Pay with Personal Check, Business Check, Money Order, or Cash.

Standard Deed: $23 First Page + $5 Each Additional Page.
NonStandard Deed: $48 First Page + $5 Each Additional Page.
_Include Self-Addressed-Stamped-Envelope for return of Original_

Standard Assignment: $23 First Item + $7.50 Each Additional Item
NonStandard Assignment: $48 First Item + $7.50 Each Additional Item
_Include Self-Addressed-Stamped-Envelope for return of Original._

Standard Margin Release: $21 First Item + $7.50 Each Additional Item
NonStandard Margin Release: $46 First Item + $7.50 Each Additional Item
_Include Self-Addressed-Stamped-Envelope for return of Original._

Plat, Survey of One Tract of Land - Recording Without Drawing,
One Sheet Not to Exceed 24” x 18”: $18.25 for One Sheet
_Include Self-Addressed-Stamped-Envelope for return of Original._

Plat, Survey of a Subdivision, Outlots, Condominiums – Recording of
Drawings and Calculations, Each Sheet Not to Exceed 24” x 18”:
$57.00 per Page of Drawings/Calculations + $10 Each Additional Page
_Include Self-Addressed-Stamped-Envelope for return of Original._
