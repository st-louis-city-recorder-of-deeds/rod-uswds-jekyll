---
title: Marriage Licenses
layout: page
call: 314-622-3257
email: marriage@stlouis-mo.gov
mail: Marriage License Dept., Recorder of Deeds, 1200 Market Street
sidenav: True
---

# {{page.title}}


The Marriage License Department is responsible for issuing new and duplicate Marriage Licenses and applications ,and provides certified and uncertified copies of any marriage license or application from St. Louis City.

* [Marriage License Request Form](/assets/documents/marriage_app_licence.pdf)
* [Free Marriage Search](http://marriage.stlouiscityrecorder.org/DirectSearch/home.html)
* [Purchase Copy of Marriage License Online](https://www.officialrecordsonline.com/Select/Index.html?state=MO&county=ST%20LOUIS%20CITY#/)


<section class="standout">
<h3>Saint Louis City – Years 1932 to Present</h3>
<h3>Applicant Qualifications</h3>
<ul>
	<li>Must be at least 18 years of age.</li>
	<li>Divorcee must wait at least 30 days from the date of divorce finalization to apply.</li>
</ul>
<h3>Application Requirements</h3>
<ul>
	<li>$48.00 Nonrefundable Application Fee by Visa/MasterCard or Cash. No checks.</li>
	<li>Both Applicants must appear together at Recorder’s Office to make the Application.</li>
	<li>Both Applicants must present their Driver’s License, State ID or Passport.</li>
	<li>6-Month Application Expiration. You must pick up License within 6 months after Application or it is no longer valid.</li>
</ul>
</section>

## License Use Requirements
* 30-Day License Use Expiration. License must be used for a ceremony within 30 days after it is has been picked up.
* Two witnesses to the ceremony must sign the License.
* Within 15 days after ceremony, clergy or judge must complete, sign and return License to Recorder of Deeds.
* Ceremony must be performed in Missouri and officiated by a U.S. citizen who is a clergy in good standing with a Missouri congregation or judge of a court of record.

<section class="standout">
<h3>Certified Copy Requirements(1932 to Present)</h3>
<ul>
	<li>$12.00 Nonrefundable Fee. Payment for Walk-In Service is by Visa/MasterCard or Cash. No Checks. Payment for Mail-In Service is by Check, Money Order or Cash.</li>
	<li>Provide Both Applicant Names (First Name + Middle Name + Last Name) <span class="caps">AND</span> Ceremony Date (Month + Day + Year).</li>
</ul>
<p><em>Marriage Records from 1766 to 1931 Are Located In Recorder of Deeds Archives Dept. – 314.589.6796 Room 129, City Hall – archives@stlouis-mo.gov</em></p>
</section>
