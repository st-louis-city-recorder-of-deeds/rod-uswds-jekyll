---
title: Birth and Death Records
layout: page
call: 314-613-3016
email: vitalrecords@stlouis-mo.gov
mail: Vital Records Dept., Recorder of Deeds, 1200 Market Street
sidenav: true
---
# {{page.title}}

The Vital Records Department provides assistance in obtaining birth or death certificates recorded anywhere within the State of Missouri.

State and Federal Law requires that a valid form of ID be required if the request is submitted in person — or that a notarized signature be require if submitted by mail.  Please call or visit our website for forms and information.


<section class="standout">
<p><a href="/assets/documents/birthform.pdf">Birth Certificates request form</a></p>
<p><a href="/assets/documents/deathform.pdf">Death Certificates request form</a></p>
</section>
<!--
<button class="usa-button usa-button--big usa-button--outline">
  <a class='text-accent-warm' href="/vitalchek.html"> Purchase Online! </a> <br/>
  <span class='font-sans-xs'>powered by vitalchek</span>
</button>
-->
### Certified Copy Requirements

* __Birth Certificates__: _$15.00 Nonrefundable Fee Per Copy._
* __Death Certificates__: _$13.00 Nonrefundable Fee for First Copy of record. $10.00 Per Each Additional Copy of the Same Record._
* Payment for Walk-In Service is by Visa/MasterCard or Cash. No Checks. Payment for Mail-In Service is by Check, Money Order, or Cash.
* Walk-In Customer Must Provide Driver’s License or State ID. Applicant’s name on the License/ID must match the Applicant’s name on Copy Application.
* Mail-In Customer Must Provide Notarized Signature and Date with this Statement:
bq. I, ______, subject to penalty of perjury, do solemnly declare and affirm that I am eligible to receive a certified copy of the vital record(s) requested and that the information contained in my request is true and correct to the best of my knowledge.

#### All Application Information Must Be Provided.

Fill out the above form

<section class="standout">
<h4>For Birth Certificates</h4>
<p>1. Birth Registrant’s Name (person’s birth record being sought: First + Middle + Last)<br>
2. Place of Birth (Hospital or Home Address)<br>
3. Date of Birth (Month + Day + Year)<br>
4. Father’s Name (First + Middle + Last)<br>
5. Mother’s Maiden Name (First + Middle + Last)<br>
6. Applicant’s Name (Person Requesting Copy)<br>
7. Applicant’s Address (Street Number + Street Name + Apartment Number + City + State + Zip Code)<br>
8. Applicant’s Phone Number.<br>
9. Relationship of Applicant to Birth Registrant or Interest of Person Requesting Copy<br>
10. Purpose for which Copy is to be used</p>
</section>
#### For Death Certificates

1. Name of Deceased (First + Middle + Last)
2. Place of Death (Hospital or Home Address)
3. Date of Death (Month + Day + Year)
4. Applicant’s Name (Person Requesting Copy)
5. Applicant’s Address (Street Number + Street Name + Apartment Number + City + State + Zip Code)
6. Applicant's Phone Number.
7. Relationship of Applicant to the Deceased or Interest of Person Requesting Copy
8. Purpose for which Copy is to be used

<section class="standout">
<h3>To Correct or Change a Birth Record or Death Record</h3>
<blockquote>
<p><em>Local vital records offices are not permitted to correct/change a Birth Record or a Death Record. Please call the State Vital Records Bureau at 573.751.6385 or write Vital Records Bureau, Missouri Health Dept., P.O. Box 570, Jefferson City MO 65102.</em></p>
</blockquote>
</section>
